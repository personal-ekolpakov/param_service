package kz.greetgo.tool_service.controller.app;

import kz.greetgo.tool_service.controller.servlet.ApplicationServlet;
import kz.greetgo.tool_service.controller.util.ApplicationContext;

public class ParamServiceApplicationInitializer {

  public void init(ApplicationContext context) {

    ApplicationServlet servlet = new ApplicationServlet();

    context.registerHttpServlet("main", servlet, "/*", -1);

  }

}
