package kz.greetgo.tool_service.controller.app;

import kz.greetgo.tool_service.controller.util.ApplicationContextOverServletContext;

import javax.servlet.ServletContainerInitializer;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Set;

public class ParamServiceApplication implements ServletContainerInitializer {

  @Override
  public void onStartup(Set<Class<?>> set, ServletContext servletContext) {

    ParamServiceApplicationInitializer initializer = new ParamServiceApplicationInitializer();

    ApplicationContextOverServletContext appContext = new ApplicationContextOverServletContext(servletContext);

    initializer.init(appContext);

  }

}
