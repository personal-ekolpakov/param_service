package kz.greetgo.tool_service.controller.servlet;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

public class ApplicationServlet extends HttpServlet {

  private final String paramsDir;

  public ApplicationServlet() {
    paramsDir = getParamsDir();
  }

  //  private static final String DEFAULT_DIR = "/data/params";
  private static final String DEFAULT_DIR = "build/params";

  private static String getParamsDir() {

    String paramServiceDir = System.getenv("PARAM_SERVICE_DIR");

    if (paramServiceDir == null) {
      return DEFAULT_DIR;
    }

    if (paramServiceDir.length() == 0) {
      return DEFAULT_DIR;
    }

    return paramServiceDir;
  }

  private File fileName(HttpServletRequest req) {
    String contextPath = req.getContextPath();
    String requestURI = req.getRequestURI();

    String name = requestURI.substring(contextPath.length());
    if (!name.startsWith("/")) {
      name = "/" + name;
    }

    return new File(paramsDir + name + ".txt");
  }

  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
    File file = fileName(req);

    byte[] value = new byte[]{};

    if (file.exists()) {
      value = Files.readAllBytes(file.toPath());
    }

    resp.getOutputStream().write(value);
    resp.flushBuffer();
  }

  @Override
  protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws IOException {

    File file = fileName(req);
    //noinspection ResultOfMethodCallIgnored
    file.getParentFile().mkdirs();

    byte[] bytes = req.getInputStream().readAllBytes();

    Files.write(file.toPath(), bytes);

  }

  @Override
  protected void doDelete(HttpServletRequest req, HttpServletResponse resp) {

    //noinspection ResultOfMethodCallIgnored
    fileName(req).delete();

  }

}
