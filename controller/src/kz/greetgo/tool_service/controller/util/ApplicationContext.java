package kz.greetgo.tool_service.controller.util;

import javax.servlet.http.HttpServlet;

public interface ApplicationContext {

  void registerHttpServlet(String servletName, HttpServlet httpServlet, String urlMapping, int loadOnStartup);

}
