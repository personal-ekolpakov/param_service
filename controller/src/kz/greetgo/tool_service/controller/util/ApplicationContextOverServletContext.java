package kz.greetgo.tool_service.controller.util;

import javax.servlet.ServletContext;
import javax.servlet.ServletRegistration;
import javax.servlet.http.HttpServlet;

public class ApplicationContextOverServletContext implements ApplicationContext {

  private final ServletContext servletContext;

  public ApplicationContextOverServletContext(ServletContext servletContext) {
    this.servletContext = servletContext;
  }

  @Override
  public void registerHttpServlet(String servletName, HttpServlet httpServlet, String urlMapping, int loadOnStartup) {
    ServletRegistration.Dynamic dynamic = servletContext.addServlet(servletName, httpServlet);
    dynamic.addMapping(urlMapping);
    dynamic.setLoadOnStartup(loadOnStartup);
  }

}
