package kz.greetgo.tool_service.tomcat_embedded.server;

import kz.greetgo.tool_service.controller.util.ApplicationContext;
import org.apache.catalina.Context;
import org.apache.catalina.startup.Tomcat;

import javax.servlet.http.HttpServlet;

public class ApplicationContextOverTomcat implements ApplicationContext {

  private final Context context;

  public ApplicationContextOverTomcat(Context context) {
    this.context = context;
  }

  @Override
  public void registerHttpServlet(String servletName, HttpServlet httpServlet, String urlMapping, int loadOnStartup) {

    Tomcat.addServlet(context, servletName, httpServlet);
    context.addServletMappingDecoded(urlMapping, servletName);

  }

}
