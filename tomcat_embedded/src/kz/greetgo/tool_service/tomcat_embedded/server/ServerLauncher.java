package kz.greetgo.tool_service.tomcat_embedded.server;

import kz.greetgo.tool_service.controller.app.ParamServiceApplicationInitializer;
import org.apache.catalina.Context;
import org.apache.catalina.LifecycleException;
import org.apache.catalina.startup.Tomcat;

import java.io.File;

public class ServerLauncher {

  public static void main(String[] args) throws Exception {
    ParamServiceApplicationInitializer initializer = new ParamServiceApplicationInitializer();

    String webappDir = "build/webapp";
    String tempDir = "build/temp";

    File webapp = new File(webappDir);
    File temp = new File(tempDir);

    //noinspection ResultOfMethodCallIgnored
    webapp.mkdirs();
    //noinspection ResultOfMethodCallIgnored
    temp.mkdirs();

    Tomcat tomcat = new Tomcat();
    tomcat.setPort(13_13);
    tomcat.getConnector();

    Runtime.getRuntime().addShutdownHook(new Thread(() -> {
      try {
        tomcat.stop();
        tomcat.destroy();
      } catch (LifecycleException e) {
        throw new RuntimeException(e);
      }
    }));

    tomcat.setBaseDir(temp.getAbsolutePath());
    Context context = tomcat.addContext("/param", webapp.getAbsolutePath());

    ApplicationContextOverTomcat appContext = new ApplicationContextOverTomcat(context);

    initializer.init(appContext);

    tomcat.start();
    tomcat.getServer().await();

    System.out.println("By");
  }

}
