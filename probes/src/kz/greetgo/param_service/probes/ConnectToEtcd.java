package kz.greetgo.param_service.probes;

import io.etcd.jetcd.ByteSequence;
import io.etcd.jetcd.Client;
import io.etcd.jetcd.ClientBuilder;
import io.etcd.jetcd.KV;
import io.etcd.jetcd.KeyValue;
import io.etcd.jetcd.kv.GetResponse;

import java.util.List;
import java.util.concurrent.CompletableFuture;

import static java.nio.charset.StandardCharsets.UTF_8;

@SuppressWarnings("UnstableApiUsage")
public class ConnectToEtcd {

  public static void main(String[] args) throws Exception {

    ClientBuilder builder = Client
      .builder()
      .endpoints("http://localhost:2379");

    try (Client client = builder.build()) {

      KV kvClient = client.getKVClient();

      ByteSequence key = ByteSequence.from("test_key".getBytes());
      ByteSequence value = ByteSequence.from("test_value".getBytes());

      kvClient.put(key, value).get();

      CompletableFuture<GetResponse> getFuture = kvClient.get(key);

      GetResponse response = getFuture.get();

      List<KeyValue> values = response.getKvs();

      for (KeyValue keyValue : values) {
        String key1 = new String(keyValue.getKey().getBytes(), UTF_8);
        String value1 = new String(keyValue.getValue().getBytes(), UTF_8);

        System.out.println("===>>> " + key1 + " = " + value1 + " <<<===");
      }

      kvClient.delete(key).get();

    }
  }

}

